# datamastery.github.io

## Generating Slides

First, you need [Pandoc 2](http://pandoc.org), Graphviz, [Java](https://openjdk.java.net/).  

> Note: if you install PP from source (recommended) the build might fail.  Try `stack upgrade` and re-run `make`.

```bash
for file in `ls *.md`; do pandoc -f markdown $file -t beamer --listings -H beamer-common.tex -o `(basename $file .md)`.pdf; done
```

Note that you'll need the latest version of LaTeX and particularly the caption package.  Update with:

```bash
sudo tlmgr update --self
sudo tlmgr update --all
```

