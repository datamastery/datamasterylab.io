---
layout: databases
title: CS 4400 - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours located in CoC 133 (103A at GTL).

Every day except last day of week:
- 13:30 - 14:30 -- Open door office hours
- 14:30 - 15:30 -- I like to attend a French class, but if you need an appointment this time is available.
- 15:30 - ??? -- If anyone shows up I'll stay as late as necessary, within reason.

<!-- Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or the TA lab if I'm not expecting anyone. -->
