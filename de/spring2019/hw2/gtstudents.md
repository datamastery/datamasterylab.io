---
layout: homework
title: GT Students
---

# GT Students

## Introduction

In this assignment you'll practice

- writing classes,
- using `datetime.date` objects in Python,
- using objects, and
- transforming data comprised of objects.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking CS 2316, the TA's and the lecturer. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, login ID, and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.


## Problem Description

You are writing a web application to manage a database of Georgia Tech students.

## Solution Description

Write a module named `gtstudents` with the classes and functions specified below.

### Required Classes and Functions

#### Classes

**`GtStudent`**

Write a class called `GtStudent` with the following instance variables:

- `gtid: str` -- 9-digit GT ID
- `gtlogin: str` -- BuzzPort login id
- `first_name: str`
- `last_name: str`
- `birth_date: datetime.date`
- `major: str`
- `hours: int` -- number of completed semester hours

These instance variables should all be initialized when you create an instance of `GtStudent`.

In addition, implement the following methods:

- A magic method that gives instances of `GtStudent` a `str` representation, whether by passing an instance to the `str()` function or echoing the value of an instance in the REPL.  See the examples below for the format of this string representation.

- A magic method that makes it possible to compare two instances of `GtStudent` for equality using operator `==` -- two instances of `GtStudent` are equal if all of their instance variables are equal,

- A magic method that makes it possible to compare two instances of `GtStudent` using operator `<`. A `GtStudent` is "less than" another if their completed semster hours is less than the other `GtStudent`'s completed semester hours.  If both students have the same hours, then a younger student is "less than" the other.

- An instance method named `age` that that takes no parameters (other than self) and returns the `GtStudent`'s age in years.

  - Note: Be careful about using the `datetime.timedelta` class due to leap years.  It's probably easier to use a different approach.

- An instance method named `year` that takes no parameters (other than self) and returns the year of the student based on the student's number of hours, that is `(hours // 30) + 1`


#### Functions

**`mk_gtstudents_dict`**

Write a function called `mk_gtstudents_dict` which takes a list of `GtStudent` instances and returns a dictionary mapping GT IDs to the `GtStudent` instance having that GT ID. If more than one GT ID appears in the list, the subsequent instances may replace earlier ones.

- Bonus: if the list contains multiple `GtStudent` instances with the same GT ID, `raise` an exception with the message "Duplicate GtStudent instances with gtid XXXXXXXXX", where XXXXXXXXX is the diplicate GT ID.

**`graduating`**

Write a function called `graduating` which takes a dictionary mapping GT IDs to `GtStudent` instances (like the one returned by `mk_gtstudents_dict`) and returns a list containing all the instances of `GtStudent` with more than 115 hours.

### `doctest`

Include the [docstring](https://www.python.org/dev/peps/pep-0257/) below as a *module* docstring in your `gtstudents` module so that you can test your code using the [doctest](https://docs.python.org/3/library/doctest.html) module.

```sh
python -m doctest -v gtstudents.py
```

If all the tests pass, then you'll have a good chance of getting a 100 on this homework.  Take a look at these docstests.  Are there any edge cases that aren't tested?

**IMPORTANT**: The provided doctstrings test the code for the output we expect.  If you change these docstrings they may not test your code the way we will test it.  We engourage you to add your own docstrings, but we recommend that you leave these docstrings as-is.

```Python
>>> import datetime as dt
>>> cartman = GtStudent("123456789", "ecartman3", "Eric", "Cartman", dt.date(2009, 7, 1), "BA", 80)
>>> kenny = GtStudent("223456789", "kmccormick3", "Kenneth", "McCormick", dt.date(2009, 4, 10), "STAC", 80)
>>> stan = GtStudent("323456789", "smarsh3", "Stanley", "Marsh", dt.date(2009, 10, 19), "IE", 85)
>>> kyle = GtStudent("423456789", "kbroflovski3", "Kyle", "Broflovski", dt.date(2009, 5, 26), "CS", 85)
>>> cartman
<Eric Cartman (123456789, ecartman3), a 9 year-old 3rd-year BA major>
>>> str(cartman)
'<Eric Cartman (123456789, ecartman3), a 9 year-old 3rd-year BA major>'
>>> stan == kyle
False
>>> stan2 = GtStudent("323456789", "smarsh3", "Stanley", "Marsh", dt.date(2009, 10, 19), "IE", 85)
>>> stan is stan2
False
>>> stan == stan2
True
>>> cartman < kenny
True
>>> stan < kenny
False
>>> cartman.age()
9
>>> cartman.year()
3
>>> boys = [cartman, kenny, stan, kyle]
>>> boys_dict = {"123456789": cartman, "223456789": kenny, "323456789": stan, "423456789": kyle}
>>> mk_gtstudents_dict(boys) == boys_dict
True
>>> bebe = GtStudent("987654321", "bstevens3", "Barabara", "Stevens", dt.date(2009, 5, 5), "EE", 116)
>>> heidi = GtStudent("887654321", "hturner3", "Heidi", "Turner", dt.date(2009, 6, 2), "PHYS", 117)
>>> annie = GtStudent("787654321", "aknitts3", "Annie", "Knitts", dt.date(2010, 1, 25), "ME", 115)
>>> wendy = GtStudent("687654321", "wtestaburger3", "Wendy", "Testaburger", dt.date(2009, 10, 2), "AE", 114)
>>> girls = [bebe, heidi, annie, wendy]
>>> graduating(mk_gtstudents_dict(boys + girls))
[<Barabara Stevens (987654321, bstevens3), a 9 year-old 4th-year EE major>, <Heidi Turner (887654321, hturner3), a 9 year-old 4th-year PHYS major>]
>>> try:
...      mk_gtstudents_dict([cartman] + boys)
... except Exception as e:
...     print(str(e))
...
Multiple students with 123456789
```

## Tips and Considerations

- Python [`datetime`](https://docs.python.org/3/library/enum.html) module
- [Python exceptions](https://docs.python.org/3/tutorial/errors.html)
- Look at the grading criteria.  Stub everything first and submit that, then fill in the details one method/function at a time.

## Grading

- (10 points) All required classes, methods and functions are present, have correct argument lists, and return values of the correct type
- (10 points) `GtStudent` class with `__init__` that initializes all the required instance attributes
- (10 points) `GtStudet` class has magic method that provides correct `str` representation in REPL
- (10 points) `GtStudet` class has magic method that provides correct `str` representation str()
- (10 points) `GtStudet` class has magic method that correctly implements value equality for operator `==`
- (10 points) `GtStudet` class has magic method that correctly implements operator `<` when students have different hours
- (10 points) `GtStudet` class has magic method that correctly implements operator `<` when students have same hours
- (10 points) `GtStudet.age` method correctly implemented
- (10 points) `GtStudet.year` method correctly implemented
- (10 points) `mk_gtstudents_dict` correctly implemented
- (10 points) `graduating` correctly implemented
- (10 points) `mk_gtstudents_dict` throws exception with correct message for first duplicate GT ID in list

## Turn-in Procedure

Submit your `gtstudents.py` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.
