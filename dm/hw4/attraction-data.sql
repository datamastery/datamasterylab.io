use attraction;

-- Only the first one is real data
insert into transit values (1, 'Tour Eiffel', 'bus_station',
                            'ChIJ64R9a-Jv5kcR-BW0JxItLhI', 48.8582627, 2.292555);
insert into transit values (2, 'Odéon', 'subway_station',
                            'abc123', 10.10, 20.20);
insert into transit values (3, 'Tuilleries', 'subway_station',
                            'abc124', 10.15, 30.20);
insert into transit values (4, 'Montmarte', 'subway_station',
                            'abc125', 12.10, 20.20);
insert into transit values (5, 'Stefansplatz', 'subway_station',
                            'abc126', 20.10, 20.20);
insert into transit values (6, 'Jungfernstieg', 'subway_station',
                            'abc127', 20.10, 21.20);

-- Only the first one is real data, except for the obviously not real part
insert into attraction values (1, 'Tour Eiffel', 'Big tower', 5, 'Avenue Anatole',
                               '75007', 'Paris', 'France', NULL, 1);
insert into attraction values (2, 'Musée Louvre', 'Big museum', 1, 'rue de Musée',
                               '75007', 'Paris', 'France', NULL, 3);
insert into attraction values (3, 'Musée D\'Orsay', 'Regular museum', 2, 'rue de Musée',
                               '75007', 'Paris', 'France', NULL, 3);
insert into attraction values (4, 'Staatsoper', 'Opera House', 1, 'Ballettsraße',
                               '1000', 'Hamburg', 'Germany', NULL, 5);
insert into attraction values (5, 'Reeperbahn', 'Street', 1, 'Reeperbahn',
                               '1000', 'Hamburg', 'Germany', NULL, 6);
