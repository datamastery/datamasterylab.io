use attraction;

-- Those of you in CS 2316 will notice that the first 3 queries are
-- needed in hw4, and they'll be useful for the CS 4400 project as
-- well.

select '1. Cities and countries of all the attractions:' as '';

select '2. Names of all attractions in Paris, France:' as '';

select '3. Name, description of the the attraction wih attraction_id 1:' as '';

select '4. Name, type of transit closest to attraction with attraction_id 1:'
  as '';

-- You'll need to join tables, group, and use an aggregate function
select '5. Cities, countries and number of transit stations that serve attractions in the database:' as '';
