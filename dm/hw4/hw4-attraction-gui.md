---
layout: homework
title: HW4 - Attraction GUI
---

# HW4 - Attraction GUI

## Introduction

In this exercise you will practice:

- writing GUI applications with PyQt, and
- writing Python code to interact with an SQL database.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking CS 2316, the TA's and the professor. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, Canvas login ID, and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all modules, classes methods, and functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.

## Problem Description

You want to provide a convenient interface to the attraction data you mined from the web in the last homework.

## Solution Description

Create a GUI application in a Python source file named `attraction_gui.py` that displays the attraction data in the attraction database you created in the previous homework.

### Set-up

You may use the schema and data you created in hw3, or you may use these:

- [attraction-schema.sql](attraction-schema.sql)
- [attraction-data.sql](attraction-data.sql)

In either case, make sure your MySQL server is running and execute these commands on your operating system command line:

```sh
mysql -u root < attraction-schema.sql
mysql -u root < attraction-data.sql
```

### Main Screen

Your application may assume that there is a MySQL database named `attraction` that has been created and populated using the set-up scripts above. `attraction_gui.py` should display a login screen that gathers MySQL database user credentials to connect to this database and display a main screen as described below.

The main screen should

- (10 points) show a list of all the cities and countries of all the attractions in the `attraction` table.
- (10 points) have "Exit" and a "Show Attractions ..." buttons.
- (10 points) when the "Exit" button is clicked, the application exits.
- (10 points) when the "Show Attractions ..." button is clicked, the "Attractions in City" dialog (see below) should be displayed.
- ( 5 points) the "Show Attractions ..." button should be disabled when no city is selected in the cities list.

The "Attractions in City" dialog should

- (10 points) show a list of all the attractions in the `attraction` table that are in the city selected on the main screen.
- (10 points) have an "OK" that, when clicked, dismisses the dialog and returns the user to the main screen.
- (10 points) have a "Show Attraction ..." button that, when clicked, shows the "Attraction" dialog (see below).
- ( 5 points) the "Show Attraction ..." button should be disabled when no attraction is selected in the attractions list.

The "Attraction" dialog should

- (10 points) display all of the details of the selected attraction.
- (10 points) have an "OK" that, when clicked, dismisses the dialog and returns the user to the "Attractions in City" dialog.

## Tips and Considerations

- You'll need to install PyMySQL with conda:

  ```sh
  $ conda install mymysql
  ```

- Example code is your friend. These examples will be particularly helpful:

  - http://datamastery.gitlab.io/code/gui/todo.py
  - http://datamastery.gitlab.io/code/gui/mysql_browser.py (You can use the login dialog as-is!)

## Turn-in Procedure

Submit your `attraction_gui.py`, `attraction-schema.sql` and `attraction-data.sql` files on Canvas. After you turn in your files, make sure to check that you have submitted the correct files that you intended to. It is highly recommended that you submit the assignment early and often in order to avoid incorrect/incomplete submissions near the deadline.

Good luck!