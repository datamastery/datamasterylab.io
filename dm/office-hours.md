---
layout: dme
title: CS 2316 - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours located in CoC 133

- Mondays, 18:00 - 19:00
- Tuesdays, 15:00 - 17:00
- Other times by appt.

Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or the TA lab if I'm not expecting anyone.

## TA Lab, CoC 107A (Fall and Spring semesters)

<iframe style="width: 90%; height: 1000px;"  src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQh6Zv3bsolzAfekGRwKY0PGLipEqGKhx_gUbxfoLcgR1osZTXnxImnc18FQ2J8f628nkU36nNDCTAQ/pubhtml?gid=0&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
