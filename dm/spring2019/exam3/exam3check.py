import importlib as imp
import sys

def load_module(loginID):
    try:
        module = imp.import_module(f"{loginID}exam3")
    except ModuleNotFoundError:
        print(f"If your loginID is {loginID}, then")
        print(f"your exam3 submission should be named {loginID}exam3.py")
        print("Fix your file name or command-line argument to this checker.")
        print("Maximum possible score: 0")
        sys.exit(0)
    except Exception as e:
        print(f"File correctly named, but error: {e}")
        print("Maximum possible score: 0")
        sys.exit(0)
    return module

def check_imports(module):
    missing_imports = []
    if not getattr(module, "np").__name__ == "numpy":
        missing_imports.append("numpy not imported as np")
    if not getattr(module, "pd").__name__ == "pandas":
        missing_imports.append("pandas not imported as pd")
    return missing_imports

def check_vars(loginID, module):
    vars = [loginID + var for var in ["exams", "hws", "scores", "hwMean",
                                      "examMean", "currentScore",
                                      "desiredGrade", "desiredScore",
                                      "requiredScore", "extraEffort", "helping",
                                       "helpingCount", "scoreSeries",
                                       "gradesDf", "gradesDfFromCsv"]]
    vars += ["rfarva3hws", "rfarva3scoreSeries", "rfarva3hwMean"]
    return [var for var in vars if var not in dir(module)]


def main(loginID):
    module = load_module(loginID)
    missing_imports = check_imports(module)
    missing_vars = check_vars(loginID, module)
    if missing_imports:
        print("The following imports are missing:", end="")
        print(missing_imports)
    else:
        print("No missing imports.")
    if missing_vars:
        print("The following variables are missing:", end="")
        print(missing_vars)
    else:
        print("No missing variables.")
    lost_points = (len(missing_imports) + len(missing_vars)) * 5
    print(f"Maximum possible score: {105 - lost_points}")

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Provide your loginID as a command-line argument")
    else:
        main(sys.argv[1])
