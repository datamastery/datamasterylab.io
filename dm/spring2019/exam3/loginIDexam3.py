########################################################################
# Instructions -- READ CAREFULLY!
#
# Each question is worth 5 points.
#
# 0. Leave all provided comments intact.
#
# 1. Rename this file, replacing "loginID" with your Canvas login ID.
#    For example, if your login id is sbhat5, then your file should be named
#    sbhat5exam3.py
#
# 2. For each of the variables below, substitute your Canvas login ID for
#    "loginID".
#
# 3. Many of these questions ask you to use values from your grades.  If they
#    aren't correct (by Canvas's records) your answer is wrong.
#
# 4. This code will be auto-graded, which means means that there is no partial
#    credit.  It also means that if your first Python statement causes the
#    remaining code not to execute, you will receive a zero.
#
# 5. This code will be auto-graded, which means means that there is no partial
#    credit.  It also means that if your first Python statement causes the
#    remaining code not to execute, you will receive a zero.
#
# 6. THIS CODE WILL BE AUTO-GRADED, WHICH MEANS MEANS THAT THERE IS NO PARTIAL
#    CREDIT.  IT ALSO MEANS THAT IF YOUR FIRST PYTHON STATEMENT CAUSES THE
#    REMAINING CODE NOT TO EXECUTE, YOU WILL RECEIVE A ZERO.
#
# 7. Test your code before you submit it.
#
# 8. Test your code before you submit it.
#
# 9. TEST YOUR CODE BEFORE YOU SUBMIT IT.

# Example:
# Assign to a variable named loginIDattitude a string expressing your attitude
# towards CS 2316.

sbhat5attitude = "CS 2316 Rawks!"

# Question 1:
# Import NumPy using the convention discussed in class.

# Question 2:
# Import Pandas using the convention discussed in class.

# Question 3:
# Assign to a variable named loginIDexams a numpy array containing the scores of
# your first two exams in this class this semester.
# If an assignment was excused, that is, you don't have a grade for it and don't have a 0,
# then use np.nan for it in the array.

# Question 4:
# Assign to a variable named loginIDhws a numpy array containing the scores of
# your first five homeworks in this class this semester.
# If an assignment was excused, that is, you don't have a grade for it and don't have a 0,
# then use np.nan for it in the array.

# Question 5:
# Assign to a variable named loginIDscores a numpy array containing the scores
# of your exams and homeworks by concatenating loginIDhws and loginIDexams.
# See https://docs.scipy.org/doc/numpy/reference/generated/numpy.concatenate.html

# Question 6:
# Assign to a variable loginIDhwMean the mean of the values in loginIDhws.
# Ignore (excuse) missing homeworks.

# Question 7:
# Assign to a variable loginIDexamMean the mean of the values in loginIDexams.
# Ignore (excuse) missing exams.

# Question 8:
# Assign to a variable named loginIDcurrentScore your current cumulative score
# calculated by the weighted sum (.2 * loginIDhwMean) + (0.8 * loginIDexamMean)

# Question 9:
# Assign to a variable named loginIDdesiredGrade a string containing the grade you
# want in this course.  For example, loginIDdesiredGrade = "A"

# Question 10:
# Assign to a variable named loginIDdesiredScore the minimum score you need
# to get loginIDdesiredGrade according to the course syllabus.

# Question 11:
# Assign to a variable named loginIDrequiredScore the minimum required weighted
# sum of scores on remaining assignments in the course, assuming we have
# completed 50% of the assignments.  Use the syllabus to determined required
# end of semester score.  The following relationship holds:
# (0.5 * loginIDcurrentScore) + (0.5 * loginIDrequiredScore) = loginIDdesiredScore

# Question 12:
# Assign to a variable loginIDextraEffort a list of numbers which are 10% greater than
# each of the values in loginIDscores, that is, each score increased by 10%.
# Although not required for this exam, you should do this with a list
# comprehension or numpy universal function, which will be required on the
# final exam.

# Question 13:
# Assign to a variable loginIDhelping a numpy array of all the scores in
# loginIdScores that are greater than loginIDdesiredScore.

# Question 14:
# Assign to a variable loginIDhelpingCount the number of scores in loginIdScores
# that are greater than loginIdDesiredScore.

# Question 15:
# Assign to a variable loginIDscoreSeries a pd.Series with indices
# ["hw0", "hw1", "hw2", "hw3", "hw4", "exam1", "exam2"] and corresponding values
# from loginIDscores

# Question 16:
# Assign to a variable rfarva3scoreSeries a pd.Series with indices
# ["hw0", "hw1", "hw2", "hw3", "hw4", "exam1", "exam2"] and corresponding values
# [100, 56, 94, 73, 45, 86, 67]

# Question 17:
# Assign to a variable rfarva3hws a pd.Series with only hw0 through hw4.  Use
# Series slicing.

# Question 18:
# Assign to a variable rfarva3hwMean the mean of the values in rfarva3hws

# Question 19:
# Assign to a variable loginIDgradesDf a pd.DataFrame with the data in
# rfarva3scoresSeries and loginIDscoreSeries.  The row indices should be the
# login IDs and the column indices should be
# ["hw0", "hw1", "hw2", "hw3", "hw4", "exam1", "exam2"].
# You'll need to use DataFrame.transpose and maybe DataFrame.append,
# depending on how you do it.

# Question 20:
# Save your loginIDgradesDf DataFrame as a CSV file with the row indices in the
# first column,
# "hw0", "hw1", "hw2", "hw3", "hw4", "exam1", "exam2" for the remaining columns,
# and a header row of
# "loginID", "hw0", "hw1", "hw2", "hw3", "hw4", "exam1", "exam2"

# Question 21:
# Assign to a variable loginIDgradesDfFromCsv a pd.DataFrame by using pd.read_csv.  loginIDgradesDfFromCsv should
# have the same structure and contents as loginIDgradesDf.

