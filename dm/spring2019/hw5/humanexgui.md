---
layout: homework
title: HumaneX GUI
---

# HumaneX GUI

## Introduction

In this assignment you'll create a simple desktop GUI for the Humane Society.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking CS 2316, the TA's and the lecturer. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, login ID, and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.


## Problem Description

You work for the Humane Society and are tasked with creating a desktop GUI application to nicely view the profiles of pets in the shelters.

## Solution Description

Write a program in `humanexgui.py` that reads data from a Humane Society database, shows pet information in a table, and shows information for each pet, possibly including a picture of the pet.  The database will use the schema created by [humanex-schema.sql](humanex-schema.sql).  Note the `x` in the name to distinguish this extended version of the Humane Society database we've seen before.  You may wish to populate your database with [humanex-data.sql](humanex-data.sql) to test your application.

### Specific Instructions

### `DbLoginDialog`

Your program should start by displaying a database login dialog like the one in [mysql_browser.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/mysql_browser.py) to get correct login credentials.  After successful database connection, the `MainWindow` should be displayed.  If the user supplies incorrect credentials or the database connection failes for some other reason, exit and print an error message to the console.

- `host` -- the hostname of the MySQL server to which to connect, should default to `localhost`
- `user` -- the username of a MySQL user on `host` that can read the `humanex` database, should default to `root`
- `password` -- the password of `user`, should default to `` (empty string)
- `database` -- the name of the `humanex` database, should default to `humanex`

Your `DbLoginDialog` should look something like this when it is first displayed:

![](dblogindialog.png)

### `ManWindow`

After successfully connecting to the database your program should read the contents of the `pet` and `shelter` tables and display `Shelter Name`, `Pet Name` and `Breed` in a `QTableView`.  Note that you'll need to join the `shelter` and `pet` tables.

- Below the table should be a button labelled "Pet Details..." that, when clicked, displays a `PetDetailsDialog` showing the details of the pet selected in the table (more below).
  - The "Pet Details..." button should be disabled if no pet is selected in the table view.
- The layout of the table and button will be similar to the layout of [scripters_gui.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/scripters_gui.py)
- The contents of the table should displayed by a [QTableView](https://pyqt.readthedocs.io/en/latest/api/qtwidgets/qtableview.html) (as it is in [scripters_gui.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/scripters_gui.py))
  - We recommend that you copy and use the `SimpleTableModel` class found in [scripters_gui.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/scripters_gui.py)
  - You should make the table view row-selectable with `table_view.setSelectionMode(QAbstractItemView.SelectRows)`

Your `MainWindow` should look something like this (note that you may have scroll bars):

![](mainwindow.png)

### `PetDetails` Dialog


Your `PetDetailsDialog`  should look something like this:

![Heidi Details](heididetails.png)


### Bonus

- In the `PetDetailsDialog`, display pet's picture, if available.  The file name of the picture is available in the `pic_url` column of the `pet` table in the database.  Note that the header in the table should be "Picture URL".

  ![Chloe Details](chloedetails.png)

- Display shelter information.

  Below the "Pet Details ..." button on the `MainWindow` display a "Shelter Details ..." button that, when clicked, displays the details for the shelter at which the selected pet is housed. The button should be disabled if no pet is selected in the table.
  
  - The `ShelterDetailsDialog` should show the shelter name and list all the pets and workers at the shelter.  Buttons for pet details and worker details should display information in dialogs (see grading section below).

## Tips and Considerations

- Start your code with the contents of [scripters_gui.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/scripters_gui.py), then add the `DbLoginDialog` from  [mysql_browser.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/mysql_browser.py), and replace the code in [scripters_gui.py](https://gitlab.com/datamastery/datamastery.gitlab.io/blob/master/code/gui/scripters_gui.py) that loads data from a CSV file with code that loads data from a MySQL database connection that you get from `DbLoginDialog` ([Slide 6](https://datamastery.gitlab.io/slides/python-db-api.pdf)).

- Useful downloads:
  - [humanex-schema.sql](humanex-schema.sql)
  - [humanex-data.sql](humanex-data.sql)
  - [chloe.jpg](chloe.jpg)
  - [dante.jpg](dante.jpg)
  - [bailey.jpg](bailey.jpg)
  - [sophie.jpg](sophie.jpg)
  
## Grading

- (10 points) `DbLoginDialog` contains default values.
- (10 points) `MainWindow` after entering db login details and clicking "OK".
- (10 points) `MainWindow` contains table showing pet data.
  - (5 points) Table has correct column labels.
  - (5 points) Table has all rows from `pet` table.
  - (5 points) Table has correct shelter names from `shelter` table.
  - (5 points) Table allows one row to be selected at a time.
- (10 points) `MainWindow` has "Pet Details..." button.
  - (5 points) "Pet Details..." button is disabled when no pet is selected in table.
  - (5 points) "Pet Details..." button is enabled when a pet is selected in table.
  - (5 points) When clicked, "Pet Details..." shows the `PetDetailsDialog`.
- (10 points) `PetDetailsDialog` is a modal dialog showing some information.
  - (5 points) Shows correct shelter name
  - (5 points) Shows correct pet name
  - (5 points) Shows correct breed
  - (5 points) Shows picture of dog, if available
  - (5 points) Is dismissed when user clicks "OK"

Bonus (these will require additional database queries):

- (5 points) `MainWindow` has "Shelter Details..." button.
  - "Shelter Details..." button is disabled when no pet is selected in table.
  - "Shelter Details..." button is enabled when a pet is selected in table.
  - When clicked, "Shelter Details..." shows the `ShelterDetailsDialog`.
- `ShelterDetailsDialog` shows some shelter details.
  - (5 points) Shows shelter name
  - (5 points) Shows a list or table of all the pets at the shelter
  - (5 points) Shows list or table of all the workers at the shelter.
  - (5 points) Has "Pet Details..." button.
    - (5 points) "Pet Details..." button is disabled when no pet is selected in table.
    - (5 points) "Pet Details..." button is enabled when a pet is selected in table.
    - (5 points) When clicked, "Pet Details..." shows the `PetDetailsDialog`.
  - (5 points) Has a "Worker schedule ..." button
    - (5 points) "Worker schedule ..." button is disabled when no worker is selected
    - (5 points) "Worker schedule ..." button is enabled when a worker is selected.
    - (5 points) When clicked, the "Worker schedule ..." button displays `WorkerScheduleDialog`
- (5 points) `WorkerScheduleDialog` shows some worker schedule information
  - (5 points) Shows worker's name
  - (5 points) Shows the names of the shelters the worker works at
  - (15 points) For each shelter the worker works at, shows a list of work days and hours the worker works at that shelter

Yes, there are 200 points available on this homework.
  
## Turn-in Procedure

Submit your `humanexgui.py` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.
