---
layout: homework
title: Pokemon
---

# Pokemon Analysis

## Introduction

In this homework you will practice

- Data Manipulation with Pandas

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking this course, the TAs and the lecturer. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name and canvasID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.

## Problem Description

You are a Pokemon Trainer in the Sinnoh region who's hit a slump, and you realize that, in order to get out of it, you need a better understanding of the Pokemon around you. So, you head to the Pokemon School in Jubilife City, and learn that Pokemon Battles are very reliant on various different statistics that you previously had no idea about. So, you decide to use Pandas to familiarize yourself with all of these new numbers and determine what you need to do to become a better pokemon trainer.

## Solution Description

You are given a csv file, [SinnohPokemonStats.csv](SinnohPokemonStats.csv), that includes a Pokemon's Pokedex Number and all of its stats:

In a Jupyter Notebook titled `pokemon-analysis` complete the following tasks.

- Create a DataFrame from the csv file called `df` and set the index column to the Pokemon's Pokedex Number

- Create a column in `df` called `Comb. Attack` that includes the sum of the Pokemon's Attack and Special Attack Stats and a column called `Comb. Defense` that includes the sum of the Pokemon's Defense and Special Defense Stats. There is a 2-line maximum to complete this task (+10)

- Create a column called `Class` that has the value "Attacking" if the Pokemon's Attack is greater than its Defense or if its Special Attack is greater than its Special Defense, and "Defensive" otherwise. There is a 1-line maximum to complete this task (+10)

- Assign a DataFrame called `summary` to a DataFrame that includes the count, mean, standard deviation, min, 25th percentile, 50th percentile, 75th percentile, and max of each numerical column. There is a 1-line maximum to complete this task (+10)

- Assign the variable `type_col` to a series of all the column types in this DataFrame. There is a 1-line maximum to complete this task (+10)

- Assign the variable `one_type` to an integer that represents the count of pokemon without secondary types. There is a 1-line maximum to complete this task (+10)

- Assign the variable `avg_speed` to an integer that represents the average speed for Pokemon with pokedex numbers between 40 and 60, There is a 1-line maximum to complete this task (+10)

- Assign the variable `type_speeds` to a Series that corresponds to the average speed for each primary type there is a 1-line maximum to complete this task (+10)

- Assign the variable `type_counts` to Series that corresponds to the count of each pokemon with each primary type. There is a 1-line maximum to complete this task (+10)

- Assign the variable `strongest_steel` to a Series that corresponds to the Primary Steel Type Pokemon with the highest attack stat. There is a 1-line maximum to complete this task (+10)

- Assign the variable `strongest_by_type` to a DataFrame that corresponds to the Pokemon with the Highest Total Stat count for each Primary Type. There is a 1-line maximum to complete this task (+10)

- Bonus: Cynthia is the Strongest Trainer in the Sinnoh Region and in order to defeat her you need to optimize your team of 6 pokemon i.e all your 6 pokemon need to have the maximum value for each desired stat and be of the optimal type.
Her Spiritomb is weak to Fast Fairy Type Pokemon
Her Roserade is weak to Attacking Psychic Type Pokemon
Her Togekiss is weak to Defensive Rock Type Pokemon
Her Lucario is weak to Special Attacking Ground Type Pokemon
Her Milotic is weak to Attacking Grass Type Pokemon
Her Garchomp is weak to Special Defensive Ice Type Pokemon
Create a DataFrame called `optimal_team` that includes the 6 Pokemon that best meet the specifications above. (+20)

## Tips and Considerations

- There is no Pokemon in our Pokedex with Flying as its primary type so don't worry if not all the types are reflected in the aggregation questions that involve primary types.
- Make sure you complete each task in a new cell in your jupyter notebook, this allows you to easily run and check your code


## Grading

Each Task is worth 10 points
The Bonus is worth 20 points
Make sure that your variable names match the directions, otherwise we won't be able to properly grade your assignment

## Submission Instructions

Attach your `pokemon-analysis.ipynb` file to your Canvas assignment submission.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.

