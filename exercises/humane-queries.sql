-- Name:
-- GTID:
-- gtLogin:

use humane;


-- Write SQL queries to answer the following questions based on the
-- humane schema in humane-schema.sql. Do not hard-code any primary or
-- foreign key values.

-- Run the humane-schema.sql and humane-data.sql scripts if you want
-- to experiment with data.

-- Queries:


-- What are all the tables in the `humane` database?
select 'Names of the tables in the human db:' as '';

-- What are the names of all the pets?
select 'Names of all the pets:' as '';

-- What are the names of the pets at Howell Mill?
select 'Names of the pets at Howell Mill:' as '';

-- What is the name of the manager of Mansell?
select 'Name of the manager of Mansell:' as '';

-- What are the names of the workers who work at Mansell?
select 'Names of the workers who work at Mansell:' as '';

-- What are the names of the workers who are employees?
select 'Names of the workers who are employees:' as '';

-- What are the names of the employees who work at Mansell?
select 'Names of the employees who work at Mansell:' as '';

-- What are the names of the workers who work on Saturdays at Mansell?
select 'Names of the workers who work on Saturdays at Mansell:' as '';

-- How many pets are there (across all the shelters)?
select 'Number of pets:' as '';

-- What are the names of the shelters and the number of pets at each shelter?
select 'Names of the shelters and the numbers of pets at them:' as '';

-- What are the names of the shelters and the numbers of workers who work at them?
select 'Names of the shelters and the numbers of workers who work at them:' as '';

-- What are the names and payroll (sum of all the shelter workers' salaries) of each shelter?
select 'Names of the shelters and the payroll at them:' as '';
