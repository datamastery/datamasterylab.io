---
layout: exercise
title: String Operations
---

## Introduction

## Solution Description

Write a module named `string_ops` (in a file named `string_ops.py`) with the following functions:

### `count_char` 

- Takes two string parameters.  The second parameter is expected to be a single character.
- Returns the number of times the second parameter occurs in the first parameter.

Examples:

```Python
>>> count_char("radar", "r")
2
>>> count_char("radar", "a")
2
>>> count_char("radar", "d")
1
```

### `is_palindrome`

- Takes a single `str` parameter.
- Returns `True` if the argument is a palindrome, `False` otherwise.
  - Whitespace in the argument should be ignored.
  - Case is not significant.
  
Examples:

```Python
>>> is_palindrome("radar")
True
>>> is_palindrome("A but tuba")
True
>>> is_palindrome("radars")
False
```

