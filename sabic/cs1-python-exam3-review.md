---
layout: sabic
title: SABIC Intro Computing
---

# Exam 3 Review

## Basic Python

See previous exams.

## Algorithm design

- Computational problems - input, output
- Function design recipe
  1. Examples
  2. Header -- type contract
  3. Description
  4. Body
  5. Test
  
## Algorithm Design Patterns

- Exhaustive search
- Generate and test
- Divide and conquer

## Searching

- The search problem
- Linear search
- Binary Search

## Sorting

- The sorting problem
- Bubble sort

## Algorithm Analysis

- Analyzing algorithm running-time
- Big-O
- Complexity classes
  - $O(lg n)$
  - $O(n)$
  - $O(n^2)$
