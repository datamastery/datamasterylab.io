---
layout: homework
title: hw1 - Guess Number
---

# Guess Number

## Introduction

In this exercise you will practice

- writing simple console programs,
- using objects of classes from the standard library,
- basic console input/output (IO), and
- control structures.

## General Instructions

**This is an individual assignment.**

Collaboration at a reasonable level will not result in substantially similar code. Students may only collaborate with fellow students currently taking this course, the TA's and the lecturer. Collaboration means talking through problems, assisting with debugging, explaining a concept, etc. You should not exchange code or write code for others.

Notes:

- Include a comment with your name, login ID, and GTID at the top of all Python files.
- *Do not wait until the last minute* to do this assignment in case you run into problems.
- Pay close attention to whether problems require you to print or return the results! Printing instead of returning or vice versa will result in a point deduction.
- Name all functions as specified in the instructions.
- Unless otherwise stated, you can assume inputs will be valid in this assignment (i.e. error checking is not required).
- In a Python module you must define a value (such as a function) before referencing it. So if you call function A from function B, the definition of function A must come before the definition of function B in the file.

## Problem Description

You like guessing secret numbers and your friends won’t play with you.

## Solution Description

Write a program in a file called `guess_number.py` that randomly chooses a secret number from 1 to 10, inclusive, and asks the user to guess the number.

- As long as the user guesses incorrectly or doesn’t enter “quit” the program should keep asking the user to keep guessing.

  - If the user guesses the correct number then, before exiting, print “Yay! You guessed it.

  - It was N.” where N is the randomly chosen secret number.

  - If the user guesses incorrectly print “Higher” or “Lower” depending on whether the secret number is higher or lower than the guess.

  - If the user quits before guessing correctly print “Lame. It was N.” where N is the randomly chosen secret number.

For this assignment you may assume that you get valid input from the user.

## Tips and Considerations

- Use the [`random`](https://docs.python.org/3/library/random.html) module to generate a random number.

- Use the built-in [`input`](https://docs.python.org/3/library/functions.html#input) function to get user input.

  - What is the type of the value returned by `input`? What do you need to do with that value to compare it to the random secret number?

## Turn-in Procedure

Submit your `guess_number.py` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.
