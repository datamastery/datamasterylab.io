---
layout: homework
title: Radix
---

# Dates

## Introduction

In this assignment you will practice:

- writing functions,
- documenting functions,
- designing algorithms, and
- how to covert between number systems.

## Problem Description

We humans represent numbers using the decimal number system, also known as *base 10*.  Computers represent numbers in the binary, or *base 2* number system (actually, data are represented as voltages, which we represent as binary numbers).  Binary digits are also called *bits*.  Assembly language programs represent numbers in hexadecimal (base 16) because a single hexidecimal number can be represented in exactly 4 bits, most memory is *byte-addressable* (a byte is 8 bits) and the "word size" of most processors and memory bus width of most memory systems is some multiple of 8 (and thus 4) bits. The base of a number system is called its *radix*.

## Solution Description

Write a module called `radix` (which you should save in a file called `radix.py`) with the following functions.  For each function, be sure to the function design recipe in [Practical Programming: An Introduction to Computer Science using Python 3.6](https://pragprog.com/book/gwpy3/practical-programming-third-edition), Chapter 3 and summarized in our slides on [Python functions](https://datamastery.gitlab.io/slides/cs1-python-functions.pdf), including the type contract using [type hints](https://docs.python.org/3/library/typing.html) and a [docstring](https://www.python.org/dev/peps/pep-0257/).

A simple algorithm for converting from decimal to any other base is given [here](https://www.electronics-tutorials.ws/binary/bin_2.html).


### `dec2bin` 


Write a function named `dec2bin` that takes a single `str` parameter containing the digits of a decimal number and returns a `str` value containing the equivalent value encoded in binary digits.

**Python REPL Examples**
 
```Python
>>> dec2bin("1")
'1'
>>> dec2bin("7")
'111'
>>> dec2bin("10")
'1010'
```

### `dec2oct` 


Write a function named `dec2oct` that takes a single `str` parameter containing the digits of a decimal number and returns a `str` value containing the equivalent value encoded in octal digits.

**Python REPL Examples**
 
```Python
>>> dec2oct("1")
'1'
>>> dec2oct("8")
'10'
>>> dec2oct("10")
'12'
```

### `dec2hex` 


Write a function named `dec2hex` that takes a single `str` parameter containing the digits of a decimal number and returns a `str` value containing the equivalent value encoded in hexadecimal digits.

**Python REPL Examples**
 
```Python
>>> dec2hex("15")
'F'
>>> dec2hex("16")
'10'
>>> dec2hex("20")
'14'
>>> dec2hex("30")
'1D'
```

### `dec2radix` 


Write a function named `dec2radix` that takes a `str` parameter containing the digits of a decimal number and an `int` parameter containing a target radix and returns a `str` value containing the equivalent value encoded in the digits of the target radix.

**Python REPL Examples**
 
```Python
>>> dec2radix("2", 2)
'10'
>>> dec2radix("20", 8)
'24'
>>> dec2radix("30", 16)
'1D'
```


### `doctest`

We will use [doctest](https://docs.python.org/3/library/doctest.html) to test your module using Python REPL examples similar to the ones provided for each function above.  You may wish to include these test cases in your function docstrings, as well as others that you come up with on your own to test edge cases.  Remember that you can run docstring on your module like this (remember that `$` is the OS command shell prompt, you don't type it):

```bash
$ python -m doctest -v radix.py
```

## Turn-in Procedure

Submit your `radix.py` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.

