---
layout: homework
title: Statistics
---

# Basic Statistics

## Introduction

In this assignment you will practice:

- writing functions,
- documenting functions,
- implementing sorting and searching algorithms, and
- composing functions.

## Problem Description



## Solution Description

Write a module called `stats` (which you should save in a file called `stats.py`) with the following functions.  For each function, be sure to use the function design recipe in [Practical Programming: An Introduction to Computer Science using Python 3.6](https://pragprog.com/book/gwpy3/practical-programming-third-edition), Chapter 3 and summarized in our slides on [Python functions](https://datamastery.gitlab.io/slides/cs1-python-functions.pdf), including the type contract using [type hints](https://docs.python.org/3/library/typing.html) and a [docstring](https://www.python.org/dev/peps/pep-0257/).


### mean

Write a function named `mean` that takes a single `List` parameter containing numbers (`float` or `int`) and returns the arithmentic mean.

**Python REPL Examples**
 
```Python
>>>  mean([1, 2 , 3, 4, 5, 6])
3.5
>>>  mean([0, 1, 2 , 3, 4, 5, 6])
3
```


### `doctest`

We will use [doctest](https://docs.python.org/3/library/doctest.html) to test your module using Python REPL examples similar to the ones provided for each function above.  You may wish to include these test cases in your function docstrings, as well as others that you come up with on your own to test edge cases.  Remember that you can run docstring on your module like this (remember that `$` is the OS command shell prompt, you don't type it):

```bash
$ python -m doctest -v radix.py
```

## Turn-in Procedure

Submit your `radix.py` file on Canvas as an attachment.  When you're ready, double-check that you have submitted and not just saved a draft.

## Verify the Success of Your Submission to Canvas

Practice safe submission! Verify that your HW files were truly submitted correctly, the upload was successful, and that your program runs with no syntax or runtime errors. It is solely your responsibility to turn in your homework and practice this safe submission safeguard.

- After submitting the files to Canvas, return to the Assignment menu option and this homework. It should show the submitted files.
- Download copies of your submitted files from the Canvas Assignment page **placing them in a new folder**.
- Re-run and test the files you downloaded from Canvas to make sure it's what you expect.
- This procedure helps guard against a few things.

    - It helps insure that you turn in the correct files.
    - It helps you realize if you omit a file or files.\footnote{Missing files will not be given any credit, and non-compiling homework solutions will receive few to zero points. Also recall that late homework will not be accepted regardless of excuse. Treat the due date with respect.  Do not wait until the last minute!
(If you do discover that you omitted a file, submit all of your files again, not just the missing one.)
    - Helps find syntax errors or runtime errors that you may have added after you last tested your code.

