---
layout: sabic
title: SABIC Intro Computing - Office Hours
---

# Office Hours

## Professor Simpkins's Office Hours

- Mondays, 15:30 - 17:30 in CoC 133
- Tuesdays and Thursdays, 16:30 - 17:30 in O'Keefe, Biltmore or Apartment by demand.
- Other times by appt.

Any time my office door is open, feel free to walk in. Email me if you need to see me, even during office hours since I won't necessarily be in my office or one of my TA labs (CoC 107 or CoC 107A) if I'm not expecting anyone.
