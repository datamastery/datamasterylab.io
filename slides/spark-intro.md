% Spark

## Apache Spark

"Unified computing engine"

- cluster manager for running programs distributed across multiple computers ("nodes")
    - Spark cluster manager, YARN, or Mesos
- libraries for parallel data processing


## Spark Applications

- Driver process runs `main()` function on a node in the cluster
    - Maintains information about the Spark application
    - Responds to user's program or input
    - Analyzes, distributes, and schedules work across executors
- Executor processes
    - Carry out work assigned by driver
    - Report state of computation to driver
 
![](spark-app-architecture.png)

## Spark Langauges and APIs

Executors run Spark code.  Driver runs user's program, which can be written in one of Spark's langauge APIs:

- Scala -- the default language; Spark is written in Scala.
- Java
- Python -- includes most of the Scala API
- SQL -- subset of ANSI SQL 2003
- R -- SparkR is part of Spark core, R community provides an alternative called spraklyr

![](spark-session-lang-apis.png)

Note: there are two APIs: a high-level "structured" API, and a low-level API.

## Spark Session

Every Spark application has exactly one associated `SparkSession`. 



